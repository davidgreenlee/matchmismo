//
//  CardGameViewController.m
//  Matchismo
//
//  Created by David Greenlee on 03/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import "CardGameViewController.h"
#import "PlayingCardDeck.h"
#import "CardMatchingGame.h"

@interface CardGameViewController ()
@property (weak, nonatomic) IBOutlet UILabel *flipsLabel;
@property (nonatomic) int flipCount;
@property (nonatomic, strong) CardMatchingGame *game;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *cardButtons;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

@end

@implementation CardGameViewController


- (void)setCardButtons:(NSArray *)cardButtons
{
    _cardButtons = cardButtons;
    [self updateUI];
}

- (void)updateUI
{
    UIImage *back = [UIImage imageNamed:@"Icon-Small-50.png"];
    
    for (UIButton *cardButton in self.cardButtons) {
        Card *card = [self.game cardAtIndex:[self.cardButtons indexOfObject:cardButton]];
        [cardButton setTitle:card.contents forState:UIControlStateSelected];
        [cardButton setTitle:card.contents forState:UIControlStateSelected|UIControlStateDisabled];
        // bitwise OR means selected AND disabled together
        // NSLog(@"= %@", card.contents);
        
        cardButton.selected = card.isFaceUp;
        cardButton.enabled = !card.isUnplayable;
        cardButton.alpha = card.isUnplayable ? 0.3 : 1.0;
        
        if (cardButton.selected) {
            [cardButton setBackgroundImage:nil forState:UIControlStateNormal]; // no image for face of card
        } else {
            [cardButton setBackgroundImage:back forState:UIControlStateNormal]; // back of card has an image
        }
    }
    self.scoreLabel.text = [NSString stringWithFormat:@"Score: %d", self.game.score];
    self.infoLabel.text = self.game.info;
}

- (CardMatchingGame *)game
{
    if (!_game) _game = [[CardMatchingGame alloc] initWithCardCount:[self.cardButtons count]
                                                          usingDeck:[[PlayingCardDeck alloc] init]
                         matchingFrom:([self.gameChoiceSegmtContrl selectedSegmentIndex] + 2)];
    return _game;
}

- (void)setFlipCount:(int)flipCount {
    _flipCount = flipCount;
    self.flipsLabel.text = [NSString stringWithFormat:@"Flips: %d", self.flipCount];
}

- (IBAction)flipCard:(UIButton *)sender {
    if ([self.gameChoiceSegmtContrl isEnabledForSegmentAtIndex:0]) {
        [self.gameChoiceSegmtContrl setEnabled:NO forSegmentAtIndex:0];
        [self.gameChoiceSegmtContrl setEnabled:NO forSegmentAtIndex:1];
    }
    [self.game flipCardAtIndex:[self.cardButtons indexOfObject:sender]];
    self.flipCount++;
    [self updateUI];
}

- (IBAction)deal:(id)sender {
    // reset everything for a new game
    if (![self.gameChoiceSegmtContrl isEnabledForSegmentAtIndex:0]) {
        [self.gameChoiceSegmtContrl setEnabled:YES forSegmentAtIndex:0];
        [self.gameChoiceSegmtContrl setEnabled:YES forSegmentAtIndex:1];
    }
    // allocate a new game i.e. generate new cards whichare all face down and reset score
    self.game = nil;
    
    // reset flipCount
    self.flipCount = 0;
    [self updateUI];
}

- (IBAction)chooseGame:(id)sender {
    // NSLog(@"changed to %d", [sender selectedSegmentIndex]);
    [self deal:nil];
}

@end
