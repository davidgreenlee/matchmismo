//
//  main.m
//  Matchismo
//
//  Created by David Greenlee on 03/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
