//
//  CardGameViewController.h
//  Matchismo
//
//  Created by David Greenlee on 03/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gameChoiceSegmtContrl;

- (IBAction)deal:(id)sender;
- (IBAction)chooseGame:(id)sender;

@end
