//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by David Greenlee on 03/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
