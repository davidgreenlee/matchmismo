//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by David Greenlee on 06/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
