//
//  Card.h
//  Matchismo
//
//  Created by David Greenlee on 05/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

@property (nonatomic, strong) NSString *contents;
@property (nonatomic, getter = isFaceUp) BOOL faceUp;
@property (nonatomic, getter = isUnplayable) BOOL unplayable;

- (int)match:(NSArray *)otherCards;

@end
