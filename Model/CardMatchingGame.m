//
//  CardMatchingGame.m
//  Matchismo
//
//  Created by David Greenlee on 07/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import "CardMatchingGame.h"

@interface CardMatchingGame()

@property (nonatomic, strong) NSMutableArray *cards; // contains type Card
@property (nonatomic, readwrite) int score;
@property (nonatomic, readwrite) NSMutableString *info;

@end

@implementation CardMatchingGame

- (NSMutableArray *)cards
{
    if (!_cards) _cards = [[NSMutableArray alloc] init]; // if nil give it an empty array
    
    return _cards;
}

// designated initializer
- (id)initWithCardCount:(NSUInteger)cardCount
              usingDeck:(Deck *)deck
           matchingFrom:(NSUInteger)numberToMatchFrom
{
    self = [super init];
    
    if (self) {
        // NSLog(@"asking for %d cards", cardCount);
        _numbertoMatchFrom = numberToMatchFrom;
        
        for (int i = 0; i < cardCount; i++) {
            Card *card = [deck drawRandomCard];
            if (!card) {
                self = nil;
            } else {
                self.cards[i] = card;
                // NSLog(@"adding %@", card.contents);
            }
        }
    }
    
    return self;
}

- (Card *)cardAtIndex:(NSUInteger)index
{
    return (index < [self.cards count]) ? self.cards[index] : nil;
}

#define FLIP_COST 1
#define MISMATCH_PENALTY 1
#define MATCH_BONUS 4

- (void)flipCardAtIndex:(NSUInteger)index
{
    Card *card = [self cardAtIndex:index];
    
    if (!card.isUnplayable) { // card still in game
        if (!card.isFaceUp) { // card is face down
            
            self.info = [NSMutableString stringWithFormat:@"%@", card.contents]; // show a card
            int initialScore = self.score;
            
            int cardFaceUpCount = 0; // count all face up cards still in game
            NSMutableArray *otherCards = [[NSMutableArray alloc] init];
            for (Card *aCard in self.cards) {
                if (aCard.isFaceUp && !aCard.isUnplayable) {
                    cardFaceUpCount++;
                    [otherCards addObject:aCard];
                }
            }
            
            if (cardFaceUpCount < self.numbertoMatchFrom -1) {
                // we need to flip more cards before matching
                [self.info appendString:@" flipped up"]; // show a card

            } else {
                // enough cards so check for any match
                int matchScore = [card match:otherCards];
                if (matchScore) {
                    for (Card *otherCard in otherCards) {
                        otherCard.unplayable = YES;
                        card.unplayable = YES;
                        [self.info appendFormat:@", %@", otherCard.contents];
                    }
                    self.score += matchScore * MATCH_BONUS;
                } else {
                    // just put one card face down if no match
                    for (Card *otherCard in otherCards) {
                        otherCard.faceUp = NO;
                        [self.info appendFormat:@", %@",otherCard.contents];
                    }
                    self.score -= MISMATCH_PENALTY;
                }
            }
            
            self.score -= FLIP_COST; // only to turn the card face up
            
            [self.info appendFormat:@" = %d points", self.score - initialScore];
        }
        card.faceUp = !card.isFaceUp;
    }
}

@end
