//
//  PlayingCard.h
//  Matchismo
//
//  Created by David Greenlee on 06/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import "Card.h"

@interface PlayingCard : Card

@property (nonatomic, strong) NSString *suit;
@property (nonatomic) NSUInteger rank;

+ (NSArray *)validSuits;
+ (NSUInteger)maxRank;

@end
