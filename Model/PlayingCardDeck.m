//
//  PlayingCardDeck.m
//  Matchismo
//
//  Created by David Greenlee on 06/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import "PlayingCardDeck.h"
#import "PlayingCard.h"

@implementation PlayingCardDeck

- (id)init {
    self = [super init];
    
    if (self) {
        for (NSString *suit in [PlayingCard validSuits]) {
            // NSLog(@"suit = %@", suit);
            for (NSUInteger rank = 0; rank < [PlayingCard maxRank]; rank++) {
                // NSLog(@"rank = %d", rank);
                PlayingCard *card = [[PlayingCard alloc] init];
                card.rank = rank;
                card.suit = suit;
                [self addCard:card atTop:YES];
                // NSLog(@"deck contains %d %@", rank, suit);
            }
        }
    }
    
    return self;
}

@end
