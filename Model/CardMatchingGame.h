//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by David Greenlee on 07/10/2013.
//  Copyright (c) 2013 David Greenlee. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Deck.h"

@interface CardMatchingGame : NSObject

@property (nonatomic, readonly) int score;
@property (nonatomic, readonly) NSMutableString *info;
@property (nonatomic) NSUInteger numbertoMatchFrom;

- (id)initWithCardCount:(NSUInteger)cardCount
              usingDeck:(Deck *)deck
           matchingFrom:(NSUInteger)numberToMatchFrom;

- (void)flipCardAtIndex:(NSUInteger)index;

- (Card *)cardAtIndex:(NSUInteger)index;

@end
